install.packages("MASS")
install.packages("ISLR")
?Boston
names(Boston)

library(MASS)
library(ISLR)
names(Boston)
?Boston
attach(Boston)
fit2 <- lm(medv ~ lstat + age , data = Boston )
summary(fit2)

fit3 <- lm(medv ~.,data=Boston)
summary(fit3)

plot(fit3)

fit4= update(fit3,~. -age-indus)
summary(fit4)

fit5=lm(medv~lstat*age,Boston)
summary(fit5)

attach(Boston)
par(mfrow=c(1,1))
points(lstat,fitted(fit5),col='red',pch=90)

fix(Boston)
fix(Carseats)
names(Carseats)
summary(Carseats)

fit1 = lm(Sales ~. +Income:Advertising+Age:Price,Carseats)
summary(fit1)
 

regplot=function(x,y,...){
  fit =lm(y~x)
  plot(x,y,...)
  abline(fit,col='red')

}

attach(Carseats)

regplot(Price,Sales,xlab="Price",ylab="Sales",col="blue",pch=20)

