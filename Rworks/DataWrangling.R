install.packages("ISLR")
library(ISLR)

rm(list=ls())
getwd()
myDirectory = "C:\\Users\\cr201692\\Desktop\\RWorks"
setwd(myDirectory)
list.files(myDirectory)
Auto=read.table("Auto.data.txt")
fix(Auto)

Auto=read.table("Auto.data.txt",header = T)
fix(Auto)

Auto = read.csv("Auto.csv")
fix(Auto)

dim(Auto)
summary(Auto)


fix(Auto)
Auto[0:4,] #Only 4 rows
Auto[,1:4] #Only 4 coloums

# Additional Graphical and Numerical Summaries

plot(Auto$cylinders,Auto$mpg)

attach(Auto)
?attach

plot(cylinders, mpg)

cylinders=as.factor(cylinders)
?as.factor


plot(cylinders,mpg)

plot(cylinders, mpg, col="red")
plot(cylinders, mpg, col="red", varwidth=T)
plot(cylinders, mpg, col="red", varwidth=T,horizontal=T)
plot(cylinders, mpg, col="red", varwidth=T, xlab="cylinders", ylab="MPG")
hist(mpg)
hist(mpg,col=2)
hist(mpg,col=2,breaks=15)

pairs(Auto)
pairs(~ mpg + displacement + horsepower + weight + acceleration, Auto)

plot(horsepower,mpg)
identify(horsepower,mpg,name)


?identify
