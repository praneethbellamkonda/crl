#install.packages("RODBC")
library(RODBC)

con <- odbcConnect("DataMining",uid="cr201692",pwd ="Today@2014")
#reading data from a view

df <- as.data.frame(sqlQuery(con,"SELECT 
      [MaritalStatus]
                             ,[Gender]
                             ,[TotalChildren]
                             ,[NumberChildrenAtHome]
                             ,[EnglishEducation]
                             ,[EnglishOccupation]
                             ,[HouseOwnerFlag]
                             ,[NumberCarsOwned]
                             ,[CommuteDistance]
                             ,[Region]
                             ,[BikeBuyer]
                             FROM [AdventureWorksDW2012].[dbo].[vTargetMail]"))

close(con)
summary(df)
#colMeans(df["NumberCarsOwned"])
# Ploting graphs

#install.packages("ggplot2",dependencies = TRUE)
library(ggplot2)
ggplot(df,aes(Region, fill= EnglishEducation))+geom_bar()

#install.packages("e1071",dependencies = T)
library(e1071)

TMNB <- naiveBayes(df[,2:10],df[,11])
TMNB$apriori
#ls()
#rm(list=ls())
#rm(list=ls())

TMNB$tables


df_pr <- as.data.frame(predict(TMNB,df,type = "raw"))
df_wp <- cbind(df,df_pr)


install.packages("party",dependencies = T)
library(party)
head(df)
TMDT <- ctree(data=df,BikeBuyer ~ MaritalStatus+Gender+TotalChildren+NumberChildrenAtHome+EnglishEducation+EnglishOccupation+HouseOwnerFlag+NumberCarsOwned+CommuteDistance+Region )


plot(TMDT,type ="simple")
