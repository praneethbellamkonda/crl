rm(list=ls())

require(ISLR)
require(MASS)

#Linear Discriminant analysis
?lda

lda.fit = lda(Direction ~Lag1+Lag2,data = Smarket, subset=Year<2005)
lda.fit
fix(Smarket)
plot(lda.fit)
Smarket.2005 = subset(Smarket,Year==2005)
lda.pred=predict(lda.fit,Smarket.2005)
class(lda.fit)
data.frame(lda.pred)[0:5,]
